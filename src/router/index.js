import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/home'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    }, {
      path: '/daiban',
      component: () => import('@/views/daiban')
    }
    ,
    {
      path: '/apply',
      component: () => import('@/views/apply')
    },
    {
      path: '/project',
      component: () => import('@/views/project')
    },
    {
      path: '/my',
      component: () => import('@/views/my')
    }
  ]
})
